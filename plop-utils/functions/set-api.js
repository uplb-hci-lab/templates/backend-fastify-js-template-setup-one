const { camelCase, paramCase } = require('change-case');
const fs = require('fs');

/**
 * @param {string} output
 * @param {PlopInquiry} data
 */
module.exports = (output, data) => {
  const { name, uniqueId } = data;
  output = '';
  const api = [];
  const apiOne = [];

  /**
   * @type {Array<string>}
   */
  const folderList = fs.readdirSync(`src/api/${paramCase(name)}`);

  for (const item of folderList) {
    const isDirectory = fs.lstatSync(`src/api/${paramCase(name)}/${item}`).isDirectory();
    if (isDirectory) {
      const newItem = item === 'delete' ? 'del' : item;
      const camelFn = camelCase(newItem);
      if (item.indexOf('-one') > 0) {
        const fn = camelCase(item.replace('-one', ''));
        output = output + `import { ${fn === 'delete' ? 'del' : fn} as ${camelFn} } from './${item}';\n`;
        apiOne.push({
          name: camelFn,
          fn
        });
      } else {
        output = output + `import { ${camelFn} } from './${item}';\n`;
        api.push(camelFn);
      }
    }
  }

  output = output + `\nexport const ${camelCase(name)} = {\n  '/${paramCase(name)}/:${camelCase(uniqueId)}': {\n    `;
  output = output + `${apiOne.map(item => `${item.fn === 'del' ? 'delete' : item.fn}: ${item.name}`).join(',\n    ')}`;
  output = output + `\n  },\n  '/${paramCase(name)}': {\n    `;
  output = output + `${api.map(item => item === 'del' ? 'delete: del' : item).join(',\n    ')}`;
  output = output + '\n  }\n};\n';

  return output;
};
