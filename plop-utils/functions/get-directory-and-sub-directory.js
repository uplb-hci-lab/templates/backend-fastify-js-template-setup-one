const fs = require('fs');
/**
* @param {String} currPath
* @returns {Array<String>}
*/

const getDirectoryAndSubdirectory = currPath => {
  const choices = [];
  const dirs = fs.readdirSync(currPath);
  for (const dir of dirs) {
    const newPath = `${currPath}/${dir}`;
    if (fs.lstatSync(newPath).isDirectory()) {
      choices.push(newPath);
      const newChoices = getDirectoryAndSubdirectory(newPath);
      for (const newDir of newChoices) {
        choices.push(newDir);
      }
    }
  }
  return choices;
};

module.exports = getDirectoryAndSubdirectory;
