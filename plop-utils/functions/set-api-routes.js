const { camelCase } = require('change-case');
const fs = require('fs');

/**
 * @param {string} output
 */
module.exports = output => {
  output = '';
  const api = [];

  /**
   * @type {Array<string>}
   */
  const folderList = fs.readdirSync('src/api');

  for (const item of folderList) {
    const isDirectory = fs.lstatSync(`src/api/${item}`).isDirectory();
    if (isDirectory) {
      output = output + `import { ${camelCase(item)} } from './${item}';\n`;
      api.push(camelCase(item));
    }
  }

  output = output + '\n/**\n* @type {{ [key: string]: { [key: string]: { [key: string]: { schema: object, fn: function } } } }}\n*/';
  output = output + `\nexport const api = {\n  ${api.join(',\n  ')}\n};\n`;
  return output;
};
