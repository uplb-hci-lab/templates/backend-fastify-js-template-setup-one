const fs = require('fs');

/**
 * @param {string} output
 */
module.exports = output => {
  output = '';
  const api = [];

  /**
   * @type {Array<string>}
   */
  const folderList = fs.readdirSync('src/models');

  const babelrc = {
    plugins: [
      ['@babel/plugin-transform-modules-commonjs', {
        lazy: true
      }]
    ]
  };

  for (const item of folderList) {
    if (item !== 'index.js') {
      // @ts-ignore
      const { code } = require('@babel/core').transformFileSync(`src/models/${item}`, babelrc);
      // eslint-disable-next-line no-eval
      const object = eval(code);
      output = output + `import { ${Object.keys(object).join(', ')} } from './${item}';\n`;
      api.push(...Object.keys(object));
    }
  }

  output = output + '\n/**\n* @type {SwaggerModelDefinition}\n*/';
  output = output + `\nexport const definitions = {\n  ${api.join(',\n  ')}\n};\n`;
  return output;
};
