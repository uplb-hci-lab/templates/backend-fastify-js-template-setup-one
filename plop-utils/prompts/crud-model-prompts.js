const apiRoutePrompts = require('./api-route-prompts');

module.exports = [
  ...apiRoutePrompts,
  {
    type: 'confirm',
    name: 'api',
    message: 'Will this be under an api subroute? `/api/{model}`',
    default: true
  },
  {
    type: 'list',
    name: 'uniqueIdType',
    message: 'What is the type of the uniqueId of the model',
    choices: ['string', 'number'],
    default: 'string'
  },
  {
    type: 'list',
    name: 'uniqueIdFormat',
    message: 'What is the format of the uniqueId of the model',
    choices: ['uuid', 'email', 'none'],
    default: 'uiid',
    /**
     * @param {PlopInquiry} data
     */
    when: (data) => {
      return data.uniqueIdType === 'string';
    }
  },
  {
    type: 'editor',
    name: 'modelProperties',
    message: 'Please put the model properties in JSON format. Please see "https://github.com/fastify/fast-json-stringify" on how to write properties\n.',
    /**
     * @param {string} text
     */
    validate: text => {
      if (typeof text === 'string') {
        return 'String is not in the right JSON format';
      }
      return true;
    },
    /**
     * @param {string} text
     */
    filter: text => {
      try {
        return JSON.parse(text);
      } catch (error) {
        return text;
      }
    },
    default: '{ "param1": { "type": "string", "nullable": true } }'
  }
];
