module.exports = [
  {
    type: 'input',
    name: 'name',
    message: 'Name of the route/model',
    /**
     * @param {string} value
     */
    validate: (value) => {
      if ((/.+/).test(value)) { return true; }
      return 'Name is Required';
    }
  },
  {
    type: 'input',
    name: 'uniqueId',
    message: 'What would be the name of the unique id of the model',
    /**
     * @param {string} value
     */
    validate: (value) => {
      if ((/.+/).test(value)) { return true; }
      return 'Unique Id is Required';
    }
  }
];
