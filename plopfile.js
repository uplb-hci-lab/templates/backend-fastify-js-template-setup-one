const { paramCase, camelCase, pascalCase, capitalCase } = require('change-case');
const docPathCreator = require('./plop-utils/functions/doc-path-creator');
const dateToday = require('./plop-utils/functions/date-today');
const filenameCreator = require('./plop-utils/functions/filename-creator');
const filenamePathCreator = require('./plop-utils/functions/filename-path-creator');
const addLicenseSnippet = require('./plop-utils/functions/add-license-snippet');

module.exports = plop => {
  // general helpers
  plop.setHelper('paramCase', paramCase);
  plop.setHelper('pascalCase', pascalCase);
  plop.setHelper('docPathCreator', docPathCreator);
  plop.setHelper('capitalCase', capitalCase);
  plop.setHelper('dateToday', dateToday);
  plop.setHelper('filenameCreator', filenameCreator);
  plop.setHelper('filenamePathCreator', filenamePathCreator);
  plop.setHelper('addLicenseSnippet', addLicenseSnippet);

  // specific helpers
  plop.setHelper('propertyUniqueId', function () {
    const { uniqueId, uniqueIdType, uniqueIdFormat } = this;
    return `${camelCase(uniqueId)}: { type: "${uniqueIdType}"${uniqueIdFormat && uniqueIdFormat !== 'none'
      ? `, format: '${uniqueIdFormat}'`
      : ''} }`;
  });

  // generator setting
  plop.setGenerator('create:file', {
    description: 'Create a new file',
    prompts: require('./plop-utils/prompts/add-file'),
    actions: require('./plop-utils/actions/add-file')
  });

  plop.setGenerator('create:doc', {
    description: 'Add a document page',
    prompts: require('./plop-utils/prompts/add-document-page'),
    actions: require('./plop-utils/actions/add-document-page')
  });

  plop.setGenerator('create:crud-model', {
    description: 'Add a complete route model',
    prompts: require('./plop-utils/prompts/crud-model-prompts'),
    actions: require('./plop-utils/actions/crud-model-actions')
  });

  plop.setGenerator('fix:api', {
    description: 'Fixes an API Route',
    prompts: require('./plop-utils/prompts/api-route-prompts'),
    actions: require('./plop-utils/actions/api-route-actions')
  });

  plop.setGenerator('clean', {
    description: 'Clean the folder structure',
    prompts: [],
    actions: require('./plop-utils/actions/clean-actions')
  });
};
