import { definitions } from './models';
import { version } from '../package.json';
import securityDefinitions from './security.json';

export const swagger = {
  info: {
    title: 'Test swagger',
    description: 'testing the fastify swagger api',
    version
  },
  schemes: ['http', 'https'],
  consumes: ['application/json'],
  produces: ['application/json'],
  definitions,
  securityDefinitions
};
