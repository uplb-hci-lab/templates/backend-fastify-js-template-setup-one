export const fileResponseTemplate = {
  415: {
    description: 'Unsupported Media Type',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'request/unsupported-media-type' },
      message: {
        type: 'string',
        value: 'The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method.'
      }
    }
  }
};
