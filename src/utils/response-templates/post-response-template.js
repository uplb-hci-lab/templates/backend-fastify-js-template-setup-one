export const postResponseTemplate = {
  413: {
    description: 'Request Entity Too Large',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'request/too-large' },
      message: {
        type: 'string',
        value: 'The server is refusing to process a request because the request entity is larger than the server is willing or able to process.'
      }
    }
  }
};
