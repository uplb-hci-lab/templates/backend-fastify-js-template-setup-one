import { User, UserUpdateRequest, UserUpdateResponse } from './user.js';

/**
* @type {SwaggerModelDefinition}
*/
export const definitions = {
  User,
  UserUpdateRequest,
  UserUpdateResponse
};
