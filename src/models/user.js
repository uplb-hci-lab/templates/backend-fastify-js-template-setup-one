export const User = {
  $id: 'User',
  type: 'object',
  required: ['id'],
  properties: {
    id: { type: 'string', format: 'uuid' },
    firstName: { type: 'string', nullable: true },
    lastName: { type: 'string', nullable: true },
    email: { type: 'string', format: 'email' }
  }
};

export const UserUpdateRequest = {
  $id: 'UserUpdateRequest',
  type: 'object',
  required: ['id'],
  properties: {
    id: User.properties.id,
    firstName: User.properties.firstName,
    lastName: User.properties.lastName
  }
};

export const UserUpdateResponse = {
  $id: 'UserUpdateResponse',
  type: 'object',
  required: ['id'],
  properties: {
    id: User.properties.id,
    firstName: User.properties.firstName,
    lastName: User.properties.lastName
  }
};

export default { User, UserUpdateRequest, UserUpdateResponse };
