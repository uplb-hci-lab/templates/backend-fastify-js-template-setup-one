import { health } from './health';
import { user } from './user';

/**
* @type {{ [key: string]: { [key: string]: { [key: string]: { schema: object, fn: function } } } }}
*/
export const api = {
  health,
  user
};
