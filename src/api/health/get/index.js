import { fn } from './fn.js';

export const get = {
  schema: {
    description: 'Endpoint for Server Health Checks',
    tags: ['general'],
    summary: 'qwerty',
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' }
        }
      }
    }
  },
  fn
};
