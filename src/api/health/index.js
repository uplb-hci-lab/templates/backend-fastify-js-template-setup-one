import { get } from './get';

export const health = {
  '/health': {
    get
  }
};
