import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';

export const get = {
  schema: {
    api: true,
    description: 'Get a list of user objects',
    tags: ['User'],
    summary: 'qwerty',
    query: {
      limit: {
        type: 'string'
      },
      offset: {
        type: 'string'
      },
      filter: {
        type: 'string'
      }
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          items: {
            type: 'array',
            items: { $ref: 'User#' }
          }
        }
      },
      ...baseResponseTemplate
    },
    security: [
      {
        apiKey: []
      }
    ]
  },
  fn
};
