import { fn } from './fn.js';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';

export const del = {
  schema: {
    api: true,
    description: 'Updates one User objects',
    tags: ['User'],
    summary: 'Updates one User object',
    body: {
      type: 'object',
      user: {
        type: 'string',
        format: 'uuid'
      }
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          user: 'UserUpdateResponse#'
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    },
    security: [
      {
        apiKey: []
      }
    ]
  },
  fn
};
