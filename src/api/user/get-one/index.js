import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';

export const get = {
  schema: {
    api: true,
    description: 'Get one user object',
    tags: ['User'],
    params: {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' }
      }
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          data: 'User#'
        }
      },
      ...baseResponseTemplate
    },
    security: [
      {
        apiKey: []
      }
    ]
  },
  fn
};
