import { fn } from './fn.js';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';

export const put = {
  schema: {
    api: true,
    description: 'Update a list of user objects',
    tags: ['user'],
    summary: 'qwerty',
    body: {
      type: 'array',
      items: 'UserUpdateRequest#'
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          items: {
            type: 'array',
            items: { $ref: 'UserUpdateResponse#' }
          }
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    },
    security: [
      {
        apiKey: []
      }
    ]
  },
  fn
};
