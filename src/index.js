import fastify from './app';

const start = async () => {
  try {
    await fastify.listen(process.env.PORT || '8080');
    const port = fastify.server.address();
    if (port) {
      fastify.log.info(`server listening on ${port}`);
    }
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
